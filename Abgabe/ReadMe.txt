Bedienungsanleitung:

1. Start des Programms
2. Über den Laden Button im ersten Tab den entsprechenden Datensatz mittels Dateiexplorer laden.
3. Zum nächsten Tab Wechseln
4. Dort aktuell noch durch Nutzerinteraktion das Anzeigen der Bilddaten initialisieren.
Mittels Slider oder Mausklick Start Punkt anwählen. Per Button Klick diesen Übernehmen.
Selbiges für Endpunkt.
5. Zum Screw Verification Tab Wechseln, dort ebenfalls via Nutzerinteraktion (Betätigen der Slider)
Ansichten Initialisieren.
Scale, Slide und Diameter freiwählbar.
