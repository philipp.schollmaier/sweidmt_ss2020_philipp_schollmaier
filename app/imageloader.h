#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include <iostream>
#include <string>
#include <math.h>
#include "applicationdata.h"
#include "mylib.h"
#include <QMouseEvent>
#include <QTime>



QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class ImageLoader : public QWidget
{
    Q_OBJECT

public:
    ImageLoader(QWidget *parent = nullptr);
    ~ImageLoader();

    void setData(ApplicationData* pData);


private:
    Ui::Widget *ui;

    // Ausgelagert in MyLib
    // Aufgabe 2_2
    // int windowing(int HU_value, int startValue, int windowWidth); // Wurde in MyLib verschoben (static)



    // Aufgabe 3_2
    void update2DView(int slice, int thresh);

    // Application Data Object soll nur einmal existieren deswegen nur Zeiger *; dieses Verfahren in allen Widgets des TabWidgets
    ApplicationData *m_pData;

    void mousePressEvent(QMouseEvent *e);

private slots:

    void updatedWindowingStart(int value);
    void updatedWindowingWidth(int value);
    void Load3d();
    void updatedSlice(int value);
    void updatedThresh(int value);
    void CalcDepth();
    void calc3d();

signals:
    void LOG(QString str);


};

//class File : public QFile
//{

//};

#endif // WIDGET_H
