#include "applicationdata.h"

ApplicationData::ApplicationData()
{
    m_pTiefenkarte = new short[glw*glh];
    m_pImageData = new short[glw*glh*gld];

    m_Startposition << 0,0,0;
    m_Endposition << 0,0,0;

}

// Destruktor
ApplicationData::~ApplicationData()
{
    // Speicher wieder freigeben
    delete[] m_pImageData;
    delete[] m_pTiefenkarte;

    //delete m_pImage3D;

}

//------------------------------------------------------------------------------------

void ApplicationData::setStartPosition(Eigen::Vector3d StartPosition)
{
    m_Startposition = StartPosition;
}


Eigen::Vector3d ApplicationData::getStartPosition()
{
    return m_Startposition;
}

void ApplicationData::setEndPosition(Eigen::Vector3d EndPosition)
{
    m_Endposition = EndPosition;
}

Eigen::Vector3d ApplicationData::getEndPosition()
{
    return m_Endposition;
}

//------------------------------------------------------------------------------------

void ApplicationData::setposavailable(int length)
{

    if (length > 0)
    {
        posavailable = true;
    }
}

bool ApplicationData::getposeavailable()
{
    return posavailable;
}

bool ApplicationData::uploadImage(QString path)
{

    // PRUEFEN, Globale Ueberfuehrung der Metadaten der Rohdaten inkl. Pointer auf raw Data selbst
    //hier wird simuliert, dass mit dem Laden der Daten entsprechende Metadaten mitgeladen werden
    m_pImage3D.width = glw;;
    m_pImage3D.height = glh;
    m_pImage3D.slices = gld;
    m_pImage3D.pixelSpacingZ = pxspZ;  // 2.0
    m_pImage3D.pixelSpacingXY = pxspXY; // 1.2695
    m_pImage3D.pImage = getImage();


    // Primitives Sicherstellen, dass falls nach einem korrekten Datensatz
    // ein Datensatz mit weniger Schichten geladen wird der Überhang vom vorherigen nicht ab bspw
    // Schicht 131 angezeigt wird.
    int i;
    for (i=0; i<(glw*glh*gld); i++)
    {
        m_pImageData[i] = 0;
    }

    // Datei oeffnen
    QFile dataFile(path);

    // Variable, die angibt ob das oeffnen geklappt hat
    bool bFileOpen = dataFile.open(QIODevice::ReadOnly);

    // Abfrage ob oeffnen funktioniert hat
    if (!bFileOpen)
    {
        return false;
    }

    // Gesamtlaenge der Datei
    int iFileSize = dataFile.size();

    // Wie viele Daten wurden eingelesen?
    int iNumberBytesRead = dataFile.read((char*)m_pImageData, glw*glh*gld*sizeof(short));

    dataFile.close();

    if (iFileSize!=iNumberBytesRead)
    {
        return false;
    }

    return true;
}



bool ApplicationData::calculationDepthMap(int threshold)
{
    // QImage depthmap(512, 512, QImage::Format_RGB32);

    for (int y = 0; y < glh ; y++)
    {
        for (int x = 0; x < glw; x++)
        {
            int depthindex = glw * y + x;  // alternatives Vorgehen hier =+1 und vorher mit 0 festlegen
            m_pTiefenkarte[depthindex] = gld - 1; // Nicht auf 255 normiert, dh 129 ist Ausgangswert
            for (int s = gld - 1; s >= 0; s--)   // von 129 bis 0 durchgehen
            {
                if (m_pImageData[depthindex + s*glw*glh] > threshold)
                {
                    // Zuweisung der aktuellen Tiefe und anschliessender Abbruch fuer aktuellen Pixel
                    m_pTiefenkarte[depthindex] = (gld-s); // Nicht auf 255 normiert, siehe oben
                    break;
                }
            }
            // depthmap.setPixel(x, y, qRgb(m_pTiefenkarte[depthindex], m_pTiefenkarte[depthindex], m_pTiefenkarte[depthindex]));
        }
    }

    return true;
}


short* ApplicationData::getImage2() // voeruebergehend abklaeren mit const short in mylib pImage Pointer
{
    return m_pImageData;
}


short* ApplicationData::getImage()
{
    return m_pImageData;
}

const short* ApplicationData::getDepthMap()
{
    return m_pTiefenkarte;
}
