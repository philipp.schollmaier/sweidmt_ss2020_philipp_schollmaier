#include "sweidmt.h"
#include "ui_sweidmt.h"

SWEIDMT::SWEIDMT(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SWEIDMT)
{
    ui->setupUi(this);


    m_pImageLoader = new ImageLoader(this);
    m_pImageLoader->setData(&m_Data);
    ui->tabWidget->addTab(m_pImageLoader, "Load Image");
    connect(m_pImageLoader, SIGNAL(LOG(QString)), this, SLOT(LOG(QString)));


    m_pView3d = new View3D(this);
    m_pView3d->setData(&m_Data);   // Klaeren ob dies so richtig ist... siehe auch header und cpp von view3d mit void setdata...
    ui->tabWidget->addTab(m_pView3d, "Trajectory Planning");
    connect(m_pView3d, SIGNAL(LOG(QString)), this, SLOT(LOG(QString)));

    m_pScrew = new screw(this);
    m_pScrew->setData(&m_Data);   // Klaeren ob dies so richtig ist... siehe auch header und cpp von view3d mit void setdata...
    ui->tabWidget->addTab(m_pScrew, "Screw Verification");
    connect(m_pScrew, SIGNAL(LOG(QString)), this, SLOT(LOG(QString)));



    QString currenttime = QTime::currentTime().toString();
    emit LOG(currenttime + ": This Software Version is not for clinical use!");
    emit LOG(currenttime + ": The Dimensions must be 512x512x256");
    emit LOG(currenttime + ": The Spacing must be 1.2695, 1.2695, 2.0");
    emit LOG(currenttime + ": Press Load 3D to select a suitable Dataset");

    connect(ui->commandLinkButton_nextstep, SIGNAL(clicked()), this,
    SLOT(nextstep()));

    connect(ui->commandLinkButton_previousstep, SIGNAL(clicked()), this,
    SLOT(previoustep()));

}

SWEIDMT::~SWEIDMT()
{
    delete ui;
    delete m_pImageLoader;
    delete m_pView3d;
    delete m_pScrew;
}


void SWEIDMT::LOG(QString str)
{
    ui->textEdit->append(str);
}


void SWEIDMT::nextstep()
{
    emit LOG(QTime::currentTime().toString() + ": " + "Next Step");
    int count = ui->tabWidget->count();
    int currentTab = ui->tabWidget->currentIndex();
    if (currentTab == count - 1)
        ui->tabWidget->setCurrentIndex(0);
    ui->tabWidget->setCurrentIndex(currentTab + 1);
}


void SWEIDMT::previoustep()
{
    emit LOG(QTime::currentTime().toString() + ": " + "Previous Step");
    int count = ui->tabWidget->count();
    int currentTab = ui->tabWidget->currentIndex();
    if (currentTab == 0)
        ui->tabWidget->setCurrentIndex(2);
    ui->tabWidget->setCurrentIndex(currentTab - 1);
}

