#include "imageloader.h"
#include "ui_ImageLoader.h"


ImageLoader::ImageLoader(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);



    //Aufgabe 3_2
    connect(ui->horizontalSlider_Start, SIGNAL(valueChanged(int)), this,
    SLOT(updatedWindowingStart(int)));

    connect(ui->horizontalSlider_Width, SIGNAL(valueChanged(int)), this,
    SLOT(updatedWindowingWidth(int)));

    // Aufgabe 3_2
    connect(ui->pushButton_Load3d, SIGNAL(clicked()), this,
    SLOT(Load3d()));

    connect(ui->verticalSlider_Slice, SIGNAL(valueChanged(int)), this,
    SLOT(updatedSlice(int)));

    // Aufgabe 4.1 Segmentation Threshold Slider
    connect(ui->verticalSlider_Thresh, SIGNAL(valueChanged(int)), this,
    SLOT(updatedThresh(int)));

    //Aufgabe 4_2
    connect(ui->pushButton_Depthmap,   SIGNAL(clicked()),  this,
    SLOT(CalcDepth()));

    //Aufgabe 4_3
    connect(ui->pushButton_3dview,   SIGNAL(clicked()),  this,
    SLOT(calc3d()));




    // Funktioenen wie Depthmap und die 3d Visualisierung werden spaeter noch in MyLib ausgelagert, werden dann notwendig für weitere Ansichten
    ui->label_image->hide();
    ui->label_Slice->hide();
    ui->label_Start->hide();
    ui->label_Width->hide();
    ui->label_Thresh->hide();
    ui->label_segmentation->hide();
    ui->label->hide();
    ui->horizontalSlider_Start->hide();
    ui->horizontalSlider_Width->hide();
    ui->verticalSlider_Slice->hide();
    ui->verticalSlider_Thresh->hide();
    ui->pushButton_3dview->hide();
    ui->pushButton_Depthmap->hide();


    // Startbild
    QPixmap startimage(":/resources/img/startimage.png");
    ui->label_startimage->setPixmap(startimage.scaled(ui->label_startimage->width(), ui->label_startimage->width(), Qt::KeepAspectRatio));



}

//------------------------------------------------------------------------------------------------------------------

ImageLoader::~ImageLoader()
{
    // Speicher wieder freigeben
    delete ui;

}

//------------------------------------------------------------------------------------------------------------------

void ImageLoader::setData(ApplicationData* pData)
{
    this->m_pData = pData;
}

//------------------------------------------------------------------------------------------------------------------

// Inputdaten laden inkl Festlegung der
void ImageLoader::Load3d()
{
    // Datei auswahlen
    QString imagePath = QFileDialog::getOpenFileName(this, "Open Image", "./", "Raw Image Files (*.raw)");

    // imagePath wird an uploadImage uebergeben und falls Rueckgabe false Error anzeigen und Funktion abbrechen
    if (!m_pData->uploadImage(imagePath))
    {
        QMessageBox::critical(this, "ATTENTION", "There is an Issue concerning the Data");

        emit LOG(QTime::currentTime().toString() + ": There is an Issue concering the Data");
        return; //vllt besser break
    }



    // Bild sofort nach dem Laden in Vorschau anzeigen wird
   // update2DView(ui->verticalSlider_Slice->value(), ui->verticalSlider_Thresh->value());

   emit LOG(QTime::currentTime().toString() + ": The Data are now ready.");
   emit LOG(QTime::currentTime().toString() + ": Please go to the next Tab: 'Planning' ");
}










// Werden nach und nach entfernt und in MyLib ueberfuehrt. Da ausgeblendet keine weitere Relevanz ab hier
//
//------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------

// Aufgabe 3_2 slice
void ImageLoader::update2DView(int slice, int thresh)
{
    QImage image(m_pData->glw, m_pData->glh, QImage::Format_RGB32);
    int index = 0 + slice*m_pData->glw*m_pData->glh;

    int windowcheck = 0;
    int failcounter = 0;
    int iGrauwert = 0;
    const short* ImageData = m_pData->getImage();

    for (int y = 0; y < m_pData->glh ; ++y)
    {
        for (int x = 0; x < m_pData->glw; ++x) {
            index += 1;
            // falls Schwellwert ueberschritten (direkter Bezug auf HU nicht auf gefensterten Wert), wird betreffender Pixel rot gefaerbt
            if (ImageData[index] > thresh) {
                image.setPixel(x, y, qRgb(255, 0, 0));
            } else {
                // windowing gibt 0, -1 oder -2 aus; iGrauwert via call by reference, dh wir uebergeben iGrauwert welcher dann in Funktion veraendert wird
                windowcheck = MyLib::windowing(ImageData[index], ui->horizontalSlider_Start->value(), ui->horizontalSlider_Width->value(), iGrauwert);
                if (windowcheck != 0)  // simpler Fehlercheck, -1 und -2 nicht ausdifferenziert
                {
                    failcounter += 1;
                }
                image.setPixel(x, y, qRgb(iGrauwert, iGrauwert, iGrauwert));
            }
        }
    }
    if (failcounter > 0)
    {
        emit LOG("Der Startwert oder die Fensterbreite scheinen inkorrekt zu sein.");
    }
    //image = image.scaled(m_pData->glw * m_pData->pxspXY, m_pData->glh * m_pData->pxspXY);
    //ui->label_image->resize(image.width(), image.height());
    ui->label_image->setPixmap(QPixmap::fromImage(image));

}

//------------------------------------------------------------------------------------------------------------------

void ImageLoader::updatedWindowingStart(int value)
{
    // Aktuellen Wert auf Label anzeigen lassen -> ueber ui->horizontalSlider_Start->value()  kann jeder Zeit der Wert des Sliders abgefragt werden
    // Ergaenze spaeter noch, dass Start und Width Anfangswerte bei Programmstart ins Labelgeschrieben werden, damit Label nicht bei Aenderung der Anfwerte haendisch aktualisiert werden muessen
    ui->label_Start->setText("Start: " + QString::number(value));
    update2DView(ui->verticalSlider_Slice->value(), ui->verticalSlider_Thresh->value());
}

//------------------------------------------------------------------------------------------------------------------

void ImageLoader::updatedWindowingWidth(int value)
{
    // Aktuellen Wert anzeigen lassen
    ui->label_Width->setText("Width: " + QString::number(value));
    update2DView(ui->verticalSlider_Slice->value(), ui->verticalSlider_Thresh->value());
}


//------------------------------------------------------------------------------------------------------------------

// Aufgabe 3_3
void ImageLoader::updatedSlice(int value)
{
    // Aktuellen Wert anzeigen lassen -> ueber ui->horizontalSlider_Start->value()  kann jeder Zeit der Wert des Sliders abgefragt werden
    ui->label_Slice->setText("Slice: " + QString::number(value));
    update2DView(ui->verticalSlider_Slice->value(), ui->verticalSlider_Thresh->value());
}

//------------------------------------------------------------------------------------------------------------------

// Aufgabe 4_1
void ImageLoader::updatedThresh(int value)
{
    ui->label_Thresh->setText("Threshold: " + QString::number(value));
    update2DView(ui->verticalSlider_Slice->value(), ui->verticalSlider_Thresh->value());
}

//------------------------------------------------------------------------------------------------------------------

// Aufgabe 4_2
void ImageLoader::CalcDepth()
{
    QImage depthmap(m_pData->glw, m_pData->glh, QImage::Format_RGB32);  // Nur noetig falls Tiefenkarte auch angezeigt werden soll

    // calculationDepthMap aus application Data, Uebergabe des Schwellwerts von Slider
    m_pData->calculationDepthMap(ui->verticalSlider_Thresh->value());

    // Berechnung der Tiefenkarte in applicationdata
    const short* DepthMap = m_pData->getDepthMap();


    for (int y = 0; y < m_pData->glh ; y++)
    {
        for (int x = 0; x < m_pData->glw; x++)
        {
            int depthindex = m_pData->glw * y + x;
            depthmap.setPixel(x, y, qRgb(DepthMap[depthindex], DepthMap[depthindex], DepthMap[depthindex]));
        }
    }

    ui->label_image->setPixmap(QPixmap::fromImage(depthmap));  // Nur noetig falls Tiefenkarte auch angezeigt werden aktuell noch zur besseren Fehlerhebung
}


//------------------------------------------------------------------------------------------------------------------

// Aufgabe 4_3
void ImageLoader::calc3d()
{
    // Tiefenkarte berechenen
    CalcDepth();
    //Tiefenkarte als DepthMap von applicationdata
    const short* DepthMap = m_pData->getDepthMap();

    int s_x = 2;
    int s_y = 2;

    QImage view3d(m_pData->glw, m_pData->glw, QImage::Format_RGB32);

    for (int y = 1; y < (m_pData->glh - 1) ; y++)  // verkleinert, um nicht mit aktueller Schrittweite den Rand zu ueberschreiten, werte hier festgelegt bald Abaendern in Vars fuer schnellere Anpassungen
    {
        for (int x = 1; x < (m_pData->glw - 1); x++)
        {
            int idx = m_pData->glw * y +x;
            int T_x = abs(DepthMap[idx + 1] - DepthMap[idx - 1]);
            int T_y = abs(DepthMap[idx + m_pData->glw] - DepthMap[idx - m_pData->glw]);
            int I_r = (255*s_x * s_y) / (sqrt(pow(s_y*T_x,2)+pow(s_x*T_y,2)+pow(s_x*s_y,2)));
            view3d.setPixel(x, y, qRgb(I_r, I_r, I_r));
        }

    }
    ui->label_image->setPixmap(QPixmap::fromImage(view3d));
}

//------------------------------------------------------------------------------------------------------------------

void ImageLoader::mousePressEvent(QMouseEvent *e)
{
    QPoint widgetPos;
    QPoint labelPos;

    if (e->button() == Qt::LeftButton)  // nur links klick
    {
        widgetPos = e->pos();  // Position im Widget noch mal genauer klaeren bzgl pos() und localPos()

        //QPoint globalPos;
        //globalPos.setX(e->x());
        //globalPos.setY(e->y());

        labelPos = widgetPos - ui->label_image->pos();

        // localPos= mapFromParent(globalPos); // noch mal klaeren ...
        int xl = labelPos.x();
        int yl = labelPos.y();


        //if (xl >= 0 && yl >= 0 && xl < ui->label_image->width() && yl < ui->label_image->height())
        if(ui->label_image->rect().contains(labelPos))
        {
            //Slice
            int zl = ui->verticalSlider_Slice->value();

            // Print 3D Coords

            QString str =  QTime::currentTime().toString() + " ( " + QString::number(xl) + " ; " + QString::number(yl) + " ; " + QString::number(zl) + " )";
            emit LOG(" " + str);
        }
    }
}
