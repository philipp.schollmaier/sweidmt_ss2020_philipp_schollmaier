#include "screw.h"
#include "ui_screw.h"

screw::screw(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::screw)
{
    ui->setupUi(this);


    connect(ui->horizontalSlider_Start, SIGNAL(valueChanged(int)), this,
    SLOT(updatedWindowingStart(int)));

    connect(ui->horizontalSlider_Width, SIGNAL(valueChanged(int)), this,
    SLOT(updatedWindowingWidth(int)));

    connect(ui->horizontalSlider_Thresh, SIGNAL(valueChanged(int)), this,
    SLOT(updatedThresh(int)));

    connect(ui->horizontalSlider_Diameter, SIGNAL(valueChanged(int)), this,
    SLOT(updatedDiameter(int)));

    connect(ui->vertical_Slider_Screw, SIGNAL(valueChanged(int)), this,
    SLOT(updatedSlice_Screw(int)));

    connect(ui->horizontalSlider_Scale, SIGNAL(valueChanged(int)), this,
    SLOT(updatedScale(int)));


    ui->label_image_screwview->setStyleSheet("color: rgb(0,0,0); "
                                       "border: 2px solid rgb(0,0,0);"
                                       "border-radius: 1px");
                                       //"background-color: rgb(0, 255, 0); "

    ui->label_image_screworthogonalview->setStyleSheet("color: rgb(0,0,0); "
                                       "border: 2px solid rgb(0,0,0);"
                                       "border-radius: 1px");
                                        // "background-color: rgb(0, 255, 0); "
}

screw::~screw()
{
    delete ui;
}

//------------------------------------------------------------------------------------------------------------------

void screw::setData(ApplicationData* pData)
{
    this->m_pData = pData;
}

//------------------------------------------------------------------------------------------------------------------

void screw::updatedWindowingStart(int value)
{
    ui->label_WindowStart->setText("Start: " + QString::number(value));

    updateScrewView(ui->vertical_Slider_Screw->value(), ui->horizontalSlider_Scale->value() * 0.1, ui->horizontalSlider_Diameter->value(), value, ui->horizontalSlider_Width->value(), ui->horizontalSlider_Thresh->value());

}

void screw::updatedWindowingWidth(int value)
{
    ui->label_WindowWidth->setText("Width: " + QString::number(value));

    updateScrewView(ui->vertical_Slider_Screw->value(), ui->horizontalSlider_Scale->value() * 0.1, ui->horizontalSlider_Diameter->value(), ui->horizontalSlider_Start->value(), value, ui->horizontalSlider_Thresh->value());
}

//------------------------------------------------------------------------------------------------------------------

void screw::updatedThresh(int value)
{
    ui->label_segthreshold->setText("Threshold: " + QString::number(value));

    updateScrewView(ui->vertical_Slider_Screw->value(), ui->horizontalSlider_Scale->value() * 0.1, ui->horizontalSlider_Diameter->value(), ui->horizontalSlider_Start->value(), ui->horizontalSlider_Width->value(), value);
}

//------------------------------------------------------------------------------------------------------------------

void screw::updatedDiameter(int diameter)
{
    ui->label_diameter->setText("Screw Diameter: " + QString::number(diameter) + " mm");

    updateScrewView(ui->vertical_Slider_Screw->value(), ui->horizontalSlider_Scale->value() * 0.1, diameter, ui->horizontalSlider_Start->value(), ui->horizontalSlider_Width->value(), ui->horizontalSlider_Thresh->value());

}

//------------------------------------------------------------------------------------------------------------------

void screw::updatedScale(int scale) // CAVE scale darf nicht 0 werden -> error handling
{

    // Beachte, dass durch Scale auch die Durchmesser Visualisierung angepasst werden muss
    ui->label_scale->setText("Scale: " + QString::number(scale*0.1) + "x");
    updateScrewView(ui->vertical_Slider_Screw->value(), scale * 0.1, ui->horizontalSlider_Diameter->value(), ui->horizontalSlider_Start->value(), ui->horizontalSlider_Width->value(), ui->horizontalSlider_Thresh->value());

}

//------------------------------------------------------------------------------------------------------------------

void screw::updatedSlice_Screw(int slice)
{
    ui->label_screwdepth->setText(QString::number(ui->vertical_Slider_Screw->value()) + " mm");

    updateScrewView(slice, ui->horizontalSlider_Scale->value() * 0.1, ui->horizontalSlider_Diameter->value(), ui->horizontalSlider_Start->value(), ui->horizontalSlider_Width->value(), ui->horizontalSlider_Thresh->value());
}




//------------------------------------------------------------------------------------------------------------------


// Finale Funktion zur Visualisierung der Screwview mit eingezeichneter Schraube
// Im nächsten Optimierungsschritt Funktion soweit wie möglich in MyLib oder ApplicationData auslagern und mit Set und Get Mehtoden arbeiten, sodass Kriterium GUI und Funktionalität getrennt vollends Beachtung findet
void screw::updateScrewView(int slice, float scale, int diameter, int WindowStart, int WindowWidth, int threshold)
{

    // Fehlerhandling, erst wenn Position (posavail==true) vorhanden, dh Start und Zielpunkt definiert sind wird Berechnung durchgeführt
    if (m_pData->getposeavailable())
    {
        // festgelegter Bildausschnitt in dem Rekonstruktion dargestellt werden soll, frei hier waehlbar
        int slicex = 512;
        int slicey = 512;

        // Start und Ziel Voxel aus zweitem Tab für Trajektorie
        Eigen::Vector3d start(m_pData->getStartPosition());
        Eigen::Vector3d end(m_pData->getEndPosition());

        Eigen::Vector3d spacing(m_pData->pxspXY, m_pData->pxspXY, m_pData->pxspZ);
        Eigen::Vector3d inv_spacing(1/m_pData->pxspXY, 1/m_pData->pxspXY, 1/m_pData->pxspZ);
        Eigen::Vector3d xdirmm;
        Eigen::Vector3d ydirmm;

        // Testvektoren
        //Eigen::Vector3d start(135,332,33);
        //Eigen::Vector3d end(192,291,11);

        // Output für Testszwecke
        //QString outputstart = QString::number(slice) + " " + QString::number(start[0]) + " " + QString::number(start[1]) + " " + QString::number(start[2]);
        //QString outputend = QString::number(slice) + " " + QString::number(end[0]) + " " + QString::number(end[1]) + " " + QString::number(end[2]);
        //emit LOG(outputstart + " " +outputend);

        // Eigen::Vector3d px2mm(m_pData->pxspXY, m_pData->pxspXY, m_pData->pxspZ);

        // Trajektorienrichtung entspricht neuer z-Richtung
        Eigen::Vector3d zdir = (end-start).normalized();
        Eigen::Vector3d zdirmm = zdir.cwiseProduct(spacing).normalized();


        // Laenge der Trajektorie; im Lastenheft 1mm Inkremente festgelegt. Falls gewaehlte Trajektorie Gleitkommazahl, Abrunden=> Abschaetzung zur sicheren Seite
        double length = (end - start).cwiseProduct(spacing).norm(); // laenge in mm
        ui->vertical_Slider_Screw->setMaximum(int(length));

        if (abs(zdirmm[1]) == 1 && zdirmm[0] == 0 && zdirmm[2] == 0)
        {
            xdirmm << -zdirmm[1],0,0 ;
        } else {
            xdirmm = zdirmm.cross(Eigen::Vector3d(0,-1,0)).normalized();
        }

        ydirmm = zdirmm.cross(xdirmm).normalized();

        Reconstruction rec;
        rec.xdir = xdirmm.cwiseProduct(inv_spacing).normalized();
        rec.ydir = ydirmm.cwiseProduct(inv_spacing).normalized();


        // BEACHTE SONDERFALL (0,1,0) und (0,-1,0) => Kreuzprodukt waere (0,0,0)
        // Sonst Kreuzprodukt mit "nach oben" zeigendem Vektor (neg. urspr. y-achse);
        // somit neue x-Achse immer innerhalb einer parallelverschobenen Ebene, die durch ursprüngl. x-z-Ebene aufgespannt

        if (scale > 0)
        {
            rec.scale = scale;
        } else if (scale <= 0) {
            rec.scale = 1;
            emit LOG(QTime::currentTime().toString() + ": Scalefactor must not equal to Zero!");
        }

        // Position in Voxel // CAVE Aktuell ist festgelegt, dass die einzelnen Slices unabhängig vom Scalingfacotr in einem Abstand von 1mm rekonstruiert werden
        rec.pos << start + slice * zdirmm.cwiseProduct(inv_spacing);
        // falls mit Scalefactor auch die Schrittinkremente angepasst werden sollen 2x -> 0.5mm Schichtabstand
        // beachte Slideranpassung, sodass dennoch entlang gesamter Trajektorie verfahren werden kann
        //rec.pos << start + 1/scale * slice * zdirmm.cwiseProduct(inv_spacing);


        image2D ImageData2D(slicex, slicey); // im2D.height und im2D.width wird hier in mylib festgelegt


        // Rekonstruktion der schraegen Ebene
        int getslicecheck = MyLib::getSlice(m_pData->m_pImage3D, rec, ImageData2D);
        if (getslicecheck == 0)
        {

        } else if (getslicecheck == -1)
        {
            emit LOG(QTime::currentTime().toString() + ": There seems to be an issue with the input image.");
            return;
        } else if (getslicecheck == -2)
        {
            emit LOG(QTime::currentTime().toString() + ": There seems to be an issue with the reconstructed 2D Image.");
            return;
        }

        // Draw the reconstructed Image, wrt. Windowing & Segmentation
        QImage image(slicex, slicey, QImage::Format_RGB32);
        int windowcheck;
        int iGrauwert = 0;
        int index = 0;
        for (int y = 0; y < slicex; ++y)
        {
            for (int x = 0; x < slicey; ++x)
            {
                if (ImageData2D.pImage[index] > threshold) {
                    image.setPixel(x, y, qRgb(0, 255, 255));  //255 - z, um knie links, huefte rechts
                } else {
                windowcheck = MyLib::windowing(ImageData2D.pImage[index], WindowStart, WindowWidth, iGrauwert);
                image.setPixel(x, y, qRgb(iGrauwert, iGrauwert, iGrauwert));
                }
              index += 1;
            }
        }

        // Indikatoren fuer Schrauben Position/Orientierung, Laenge & Ansicht
        QPainter painter(&image);
        //https://doc.qt.io/qt-5/coordsys.html
        //painter.setRenderHint(QPainter::Antialiasing, true);
        painter.setPen(QPen(Qt::red, 1, Qt::SolidLine, Qt::FlatCap));
        // Bildmitte entspricht Schraubenpos, bei gerader Pixelanzahl liegt rekonstruierte Bildmitte um einen Pixel nach re bzw unten versetzt

        QPointF center(int(slicex/2), int(slicey/2));
        // https://doc.qt.io/qt-5/qpainter.html#drawEllipse (center, xradii, yradii)
        painter.drawEllipse(center, diameter/2*scale, diameter/2*scale);


        painter.setPen(QPen(Qt::green, 1, Qt::SolidLine, Qt::FlatCap));
        painter.drawLine(center + QPointF(0, diameter/2 * scale), center - QPointF(0, diameter/2 * scale));
        painter.drawLine(center + QPointF(diameter/2 * scale, 0), center - QPointF(diameter/2 * scale, 0));

        ui->label_image_screwview->setPixmap(QPixmap::fromImage(image));



        // Show orthogonal View, verschiedenes Optimierungspotential, ggf hier einfach bereits definierte Parameter mit uebergeben, als erster leichter Schritt
        updateOrthogonalScrewView(slice, scale, diameter, WindowStart, WindowWidth, threshold);



    } else {
        // Noch zu optimieren, da Fehlermeldung durch jedes Inkrement des Sliders jedes Mal ausgegeben wird.
        // ggf Slider erst mit von 0-0 initialisieren und nach Festlegen einer Trajektrorie erweitern, oder ueber QMessageBox, wie bei imageLoader
        emit LOG(QTime::currentTime().toString() + ": Please define a Start Position and an End Position in the previous Tab.");
        return;
    }
}

//------------------------------------------------------------------------------------------------------------------


// OPTIMIEREN: Funktionen zusammenfuehren aktuell noch sehr redundant zu updateScewView lediglich x und z direction getauscht
void screw::updateOrthogonalScrewView(int slice, float scale, int diameter, int WindowStart, int WindowWidth, int threshold)
{
    // Analog zu Screwview, Optimierungpotential
    if (m_pData->getposeavailable())
    {

        // Intendierte Groesse des Bildes auf dem rekonstruiert werden soll
        int slicex = 512;
        int slicey = 512;

        // Start und Ziel Voxel aus zweitem Tab für Trajektorie
        Eigen::Vector3d start(m_pData->getStartPosition());
        Eigen::Vector3d end(m_pData->getEndPosition());

        Eigen::Vector3d spacing(m_pData->pxspXY, m_pData->pxspXY, m_pData->pxspZ);
        Eigen::Vector3d inv_spacing(1/m_pData->pxspXY, 1/m_pData->pxspXY, 1/m_pData->pxspZ);
        Eigen::Vector3d xdirmm;
        Eigen::Vector3d ydirmm;
        Eigen::Vector3d zdirmm;


        // x-dir und z-dir getauscht, sodass Ansicht entlang der Trajektorie moeglich
        Reconstruction rec;
        Eigen::Vector3d zdir;


        rec.xdir = (end-start).normalized();
        xdirmm = rec.xdir.cwiseProduct(spacing).normalized();

        double length = (end - start).cwiseProduct(spacing).norm(); // laenge in mm


        if (abs(xdirmm[1]) == 1 && xdirmm[0] == 0 && xdirmm[2] == 0)
        {
            zdirmm << -xdirmm[1],0,0 ;
        } else {
            zdirmm = xdirmm.cross(Eigen::Vector3d(0,-1,0)).normalized();
        }

        ydirmm = xdirmm.cross(zdirmm).normalized();


        rec.xdir = xdirmm.cwiseProduct(inv_spacing).normalized();
        rec.ydir = ydirmm.cwiseProduct(inv_spacing).normalized();



        // CAVE SCALE Darf nicht 0 werden
        if (scale > 0)
        {
            rec.scale = scale;
        } else {
            rec.scale = scale;
         //   emit LOG("Scale must be unequal to Zero!");
        }

        // Pos in Voxel in Mitte
        rec.pos << start + xdirmm.cwiseProduct(inv_spacing) * length/2;


        image2D ImageData2D(slicex, slicey); // im2D.height und im2D.width wird hier in mylib festgelegt



        // Rekonstruktion der schraegen Ebene
        int getslicecheck = MyLib::getSlice(m_pData->m_pImage3D, rec, ImageData2D);
        if (getslicecheck == 0)
        {

        } else if (getslicecheck == -1)
        {
            emit LOG(QTime::currentTime().toString() + ": There seems to be an issue with the input image.");
            return;
        } else if (getslicecheck == -2)
        {
            emit LOG(QTime::currentTime().toString() + ": There seems to be an issue with the reconstructed 2D Image.");
            return;
        }


        QImage image(slicex, slicey, QImage::Format_RGB32);

        int windowcheck;
        int iGrauwert = 0;
        int index = 0;
        for (int y = 0; y < slicex; ++y)
        {
            for (int x = 0; x < slicey; ++x)
            {
                if (ImageData2D.pImage[index] > threshold) {
                    image.setPixel(x, y, qRgb(0, 255, 255));
                } else {
                    windowcheck = MyLib::windowing(ImageData2D.pImage[index], WindowStart, WindowWidth, iGrauwert);
                    image.setPixel(x, y, qRgb(iGrauwert, iGrauwert, iGrauwert));
                }
                index += 1;
            }
        }

        // Visualisierung
        QPainter painter(&image);
        QPointF center(int(slicex/2), int(slicey/2));
        painter.setPen(QPen(Qt::red, 1, Qt::SolidLine, Qt::FlatCap));
        painter.drawLine(center - QPointF((length/2 - slice) * scale, diameter/2 * scale), center - QPointF((length/2 - slice) * scale, -diameter/2 * scale) );

        painter.setPen(QPen(Qt::green, 1, Qt::SolidLine, Qt::FlatCap));
        painter.drawLine(center + QPointF(length/2 * scale,0) ,center - QPointF(length/2 * scale,0));

        ui->label_image_screworthogonalview->setPixmap(QPixmap::fromImage(image));

    } else {
        // Siehe ScrewView Funktion dort wird Fehlerausgabe getätigt
        // Noch zu optimieren, da Fehlermeldung durch jedes Inkrement des Sliders jedes Mal ausgegeben wird.
        // ggf Slider erst mit von 0-0 initialisieren und nach Festlegen einer Trajektrorie erweitern
        return;
    }
}
