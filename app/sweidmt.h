#ifndef SWEIDMT_H
#define SWEIDMT_H

#include <QWidget>
#include "imageloader.h"
#include "applicationdata.h"
#include "view3d.h"
#include "screw.h"
#include <QTime>


namespace Ui {
class SWEIDMT;
}

class SWEIDMT : public QWidget
{
    Q_OBJECT

public:
    explicit SWEIDMT(QWidget *parent = nullptr);
    ~SWEIDMT();


private:
    Ui::SWEIDMT *ui;

    ImageLoader *m_pImageLoader;
    View3D *m_pView3d;
    screw *m_pScrew;


    ApplicationData m_Data;




private slots:
    void LOG(QString str);

    void nextstep();
    void previoustep();

};

#endif // SWEIDMT_H
