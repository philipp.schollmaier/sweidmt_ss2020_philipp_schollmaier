#include "view3d.h"
#include "ui_view3d.h"

View3D::View3D(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::View3D)
{
    ui->setupUi(this);

    connect(ui->pushButton_SetStartPos, SIGNAL(clicked()), this,
    SLOT(setStartPos()));

    connect(ui->pushButton_SetEndPos, SIGNAL(clicked()), this,
    SLOT(setEndPos()));


    connect(ui->horizontalSlider_Start, SIGNAL(valueChanged(int)), this,
    SLOT(updatedWindowingStart(int)));

    connect(ui->horizontalSlider_Width, SIGNAL(valueChanged(int)), this,
    SLOT(updatedWindowingWidth(int)));

    connect(ui->verticalSlider_TransversalSlice, SIGNAL(valueChanged(int)), this,
    SLOT(updatedSlice_transversal(int)));

    connect(ui->verticalSlider_SagittalSlice,SIGNAL(valueChanged(int)), this,
    SLOT(updatedSlice_sagittal(int)));

    connect(ui->verticalSlider_CoronalSlice,SIGNAL(valueChanged(int)), this,
    SLOT(updatedSlice_coronal(int)));

    connect(ui->horizontalSlider_Thresh, SIGNAL(valueChanged(int)), this,
    SLOT(updatedThresh(int)));



    ui->label_image_transversal->setStyleSheet("border: 2px solid rgb(0, 0, 255);"
                                       "border-radius: 1px");

    ui->label_image_sagittal->setStyleSheet("border: 2px solid rgb(0, 255, 0);"
                                       "border-radius: 1px");

    ui->label_image_coronal->setStyleSheet("border: 2px solid rgb(255, 255, 0);"
                                       "border-radius: 1px");


}

//-----------------------------------------------------------------------------------------------

View3D::~View3D()
{
    delete ui;
}

//-----------------------------------------------------------------------------------------------

void View3D::setData(ApplicationData* pData)
{
    this->m_pData = pData;
}

//-----------------------------------------------------------------------------------------------

void View3D::setInitializationValues()
{
    // Event ist onvalue change, ueberprüfen ob es noch andere events gibt
    ui->verticalSlider_TransversalSlice->setValue(0);
    ui->verticalSlider_CoronalSlice->setValue(0);
    ui->verticalSlider_SagittalSlice->setValue(0);
}


//-----------------------------------------------------------------------------------------------

void View3D::mousePressEvent(QMouseEvent *e)
{
    QPoint widgetPos;
    QPoint transversal_label_pos;
    QPoint sagittal_label_pos;
    QPoint coronal_label_pos;

    if (e->button() == Qt::LeftButton)  // einzelner Linksklick
    {
        widgetPos = e->pos();  // Position im Widget noch mal genauer klaeren bzgl pos() und localPos()

        transversal_label_pos = widgetPos - ui->label_image_transversal->pos();
        sagittal_label_pos = widgetPos - ui->label_image_sagittal->pos();
        coronal_label_pos = widgetPos - ui->label_image_coronal->pos();
        //QPoint globalPos;
        //globalPos.setX(e->x());
        //globalPos.setY(e->y());


        //if (widgetPos >= ui->label_image_transversal->pos() && widgetPos <=)
        if (ui->label_image_transversal->rect().contains(transversal_label_pos))
        {
            ui->verticalSlider_SagittalSlice->setValue(transversal_label_pos.x());
            ui->verticalSlider_CoronalSlice->setValue(transversal_label_pos.y());

        } else if (ui->label_image_sagittal->rect().contains(sagittal_label_pos))
        {
            ui->verticalSlider_TransversalSlice->setValue(round(sagittal_label_pos.x() * m_pData->pxspXY/m_pData->pxspZ));
            ui->verticalSlider_CoronalSlice->setValue(sagittal_label_pos.y());

        } else if (ui->label_image_coronal->rect().contains(coronal_label_pos))
        {
            ui->verticalSlider_TransversalSlice->setValue(round( (ui->label_image_coronal->height() - coronal_label_pos.y()) * m_pData->pxspXY / m_pData->pxspZ));
            ui->verticalSlider_SagittalSlice->setValue(coronal_label_pos.x());
        }
    }
}

//------------------------------------------------------------------------------------------------------------------

void View3D::setStartPos()
{
    Eigen::Vector3d StartPosition(ui->verticalSlider_SagittalSlice->value(), ui->verticalSlider_CoronalSlice->value(), m_pData->gld - (ui->verticalSlider_TransversalSlice->value() + 1));
    Eigen::Vector3d spacing(m_pData->pxspXY, m_pData->pxspXY, m_pData->pxspZ);
    double length = (m_pData->getEndPosition() - StartPosition).cwiseProduct(spacing).norm();

    m_pData->setposavailable(length);



    if (m_pData->getEndPosition() != StartPosition)
    {

        m_pData->setStartPosition(StartPosition);
        QString output_pos = QString::number(StartPosition[0]) + " " + QString::number(StartPosition[1]) + " " + QString::number(StartPosition[2]);
        ui->label_StartPos->setText(output_pos);
        emit LOG(QTime::currentTime().toString() + ": StartPos: " + output_pos);


        QString output_length = "Trajectory Length [mm]: " + QString::number(length);
        ui->label_length->setText(output_length);
        emit LOG(QTime::currentTime().toString() + ": Trajectory Length [mm]: " + output_length);

    } else {
        emit LOG(QTime::currentTime().toString() + ": The choosen Startposition equals to the Endposition, please select another coordinate.");
    }






}

//------------------------------------------------------------------------------------------------------------------

void View3D::setEndPos()
{
    Eigen::Vector3d EndPosition(ui->verticalSlider_SagittalSlice->value(), ui->verticalSlider_CoronalSlice->value(), m_pData->gld - (ui->verticalSlider_TransversalSlice->value() + 1));
    Eigen::Vector3d spacing(m_pData->pxspXY, m_pData->pxspXY, m_pData->pxspZ);
    double length = (m_pData->getStartPosition() - EndPosition).cwiseProduct(spacing).norm();

    m_pData->setposavailable(length);

    if (m_pData->getStartPosition() != EndPosition)
    {
        m_pData->setEndPosition(EndPosition);
        QString output_pos = QString::number(EndPosition[0]) + " " + QString::number(EndPosition[1]) + " " + QString::number(EndPosition[2]);
        ui->label_EndPos->setText(output_pos);
        emit LOG(QTime::currentTime().toString() + ": EndPos: " + output_pos);


        QString output_length = "Trajectory Length [mm]: " + QString::number(length);
        ui->label_length->setText(output_length);
        emit LOG(QTime::currentTime().toString() + ": Trajectory Length [mm]: " + output_length);



    } else {
        emit LOG(QTime::currentTime().toString() + ": The choosen Endposition equals to the Startposition, please select another coordinate.");
    }



}


//-----------------------------------------------------------------------------------------------


void View3D::updatedSlice_transversal(int value)
{
    ui->label_transversalslice->setText("Slice: " + QString::number(value));
    updateViews(value, ui->verticalSlider_SagittalSlice->value(), ui->verticalSlider_CoronalSlice->value(), ui->horizontalSlider_Start->value(), ui->horizontalSlider_Width->value(), ui->horizontalSlider_Thresh->value());
}

//-----------------------------------------------------------------------------------------------

void View3D::updatedSlice_sagittal(int value)
{
    ui->label_sagittalslice->setText("Slice: " + QString::number(value));
    updateViews(ui->verticalSlider_TransversalSlice->value(), value, ui->verticalSlider_CoronalSlice->value(), ui->horizontalSlider_Start->value(), ui->horizontalSlider_Width->value(), ui->horizontalSlider_Thresh->value());
}


//-----------------------------------------------------------------------------------------------


void View3D::updatedSlice_coronal(int value)
{
    ui->label_coronalslice->setText("Slice: " + QString::number(value));
    updateViews(ui->verticalSlider_TransversalSlice->value(), ui->verticalSlider_SagittalSlice->value(), value, ui->horizontalSlider_Start->value(), ui->horizontalSlider_Width->value(), ui->horizontalSlider_Thresh->value());
}



//-----------------------------------------------------------------------------------------------


void View3D::updatedWindowingStart(int value)
{
    ui->label_Start->setText("Start: " + QString::number(value));
    updateViews(ui->verticalSlider_TransversalSlice->value(), ui->verticalSlider_SagittalSlice->value(), ui->verticalSlider_CoronalSlice->value(), value, ui->horizontalSlider_Width->value(), ui->horizontalSlider_Thresh->value());

}

//-----------------------------------------------------------------------------------------------

void View3D::updatedWindowingWidth(int value)
{
    ui->label_Width->setText("Width: " + QString::number(value));
    updateViews(ui->verticalSlider_TransversalSlice->value(), ui->verticalSlider_SagittalSlice->value(), ui->verticalSlider_CoronalSlice->value(), ui->horizontalSlider_Start->value(), value, ui->horizontalSlider_Thresh->value());
}

//-----------------------------------------------------------------------------------------------

void View3D::updatedThresh(int value)
{
    ui->label_Thresh->setText("Threshold: " + QString::number(value));
    updateViews(ui->verticalSlider_TransversalSlice->value(), ui->verticalSlider_SagittalSlice->value(), ui->verticalSlider_CoronalSlice->value(), ui->horizontalSlider_Start->value(), ui->horizontalSlider_Width->value(), value);
}


//-----------------------------------------------------------------------------------------------

void View3D::updateViews(int transversalslice, int sagittalslice, int coronalslice, int windowstart, int windowwidth, int threshold)
{
    update2DView_transversal(transversalslice, sagittalslice, coronalslice, windowstart, windowwidth, threshold);
    update2DView_sagittal(transversalslice, sagittalslice, coronalslice, windowstart, windowwidth, threshold);
    update2DView_coronal(transversalslice, sagittalslice, coronalslice, windowstart, windowwidth, threshold);
}

//-----------------------------------------------------------------------------------------------

// Für die drei Ansichten aequivalente Funktionen, als bald moeglich in eine parametrisierte Funktion ueberfuehren
// Auch für diese Ansichten kann getslice() verwendet werden, jedoch liefert scaled Funktion von QImage sauberer Kanten in der Ansicht
void View3D::update2DView_transversal(int transversalslice, int sagittalslice, int coronalslice, int windowstart, int windowwidth, int threshold)
{
    QImage image(m_pData->glw, m_pData->glh, QImage::Format_RGB32);

    transversalslice = (m_pData->gld-1) - transversalslice; // damit Sicht von knie -> huefte

    int index = 0 + transversalslice*m_pData->glw*m_pData->glh;

    int windowcheck = 0;
    int failcounter = 0;
    int iGrauwert = 0;
    short* ImageData = m_pData->getImage();
    for (int y = 0; y < m_pData->glh ; ++y)
    {
        for (int x = 0; x < m_pData->glw; ++x) {
            // falls Schwellwert ueberschritten (direkter Bezug auf HU nicht auf gefensterten Wert), wird betreffender Pixel rot gefaerbt
            if (ImageData[index] > threshold) {
                image.setPixel(x, y, qRgb(0, 255, 255));
            } else {
              // windowing gibt 0, -1 oder -2 aus; iGrauwert via call by reference, dh wir uebergeben iGrauwert welcher dann in Funktion veraendert wird
                windowcheck = MyLib::windowing(ImageData[index], windowstart, windowwidth, iGrauwert);
                if (windowcheck != 0)  // simpler Fehlercheck, -1 und -2 nicht ausdifferenziert
                {
                    failcounter += 1;
                }
                image.setPixel(x, y, qRgb(iGrauwert, iGrauwert, iGrauwert));
            }
            index += 1;
        }
    }

    // Errorhandling noch auslagern, damit nicht multiple Ausgaben erfolgen, immer wenn die Funktion durch das ueberstreifen von mehreren Werten im Slider aufgerufen wird und genauere Angaben zum Fehler gemacht werden kann
    if (failcounter > 0)
    {
        emit LOG(QTime::currentTime().toString() + ": There is an issue while applying the Windowing Function to the Transversal View. The Start oder the Width value seems to be out of range.");
    }


    // Falls Skalierung angepasst werden soll
    image = image.scaled(m_pData->glw * m_pData->pxspXY / m_pData->pxspXY, m_pData->glh * m_pData->pxspXY / m_pData->pxspXY);
    //imagetransversal = imagetransversal->scaled(m_pData->glw * m_pData->pxspXY/m_pData->pxspXY, m_pData->glh * m_pData->pxspXY/m_pData->pxspXY);
    //ui->label_image_transversal->resize(image.width(), image.height());




    // Indicator Coronal in Transversalview
    QPainter painter(&image);
    painter.setPen(QPen(Qt::yellow, 1, Qt::SolidLine , Qt::FlatCap));
    painter.drawLine(QPointF(0,coronalslice), QPointF(image.width(), coronalslice));

    // Indicator Sagittal in Transversalview
    painter.setPen(QPen(Qt::green, 1, Qt::SolidLine , Qt::FlatCap));
    painter.drawLine(QPointF(sagittalslice,0), QPointF(sagittalslice, image.height()));

    // Ausgabe erfolgt mit einem Pixelabstand gleich dem 1,2695mm
    ui->label_image_transversal->setPixmap(QPixmap::fromImage(image));

}


//-----------------------------------------------------------------------------------------------

// Auch für diese Ansichten kann getslice() verwendet werden, jedoch liefert scaled Funktion von QImage sauberer Kanten in der Ansicht
void View3D::update2DView_sagittal(int transversalslice, int sagittalslice, int coronalslice, int windowstart, int windowwidth, int threshold)
{
    QImage image(m_pData->gld, m_pData->glw, QImage::Format_RGB32);
    short* ImageData = m_pData->getImage();
    int index = sagittalslice;
    int windowcheck = 0;
    int failcounter = 0;
    int iGrauwert = 0;

    for (int z = 0; z < m_pData->gld; ++z) // Bildursprung oben links auessere Schleife von links nach rechts, innere Schleife von oben nach unten
    {
        for (int y = 0; y < m_pData->glw; ++y)
        {

            // falls Schwellwert ueberschritten (direkter Bezug auf HU nicht auf gefensterten Wert), wird betreffender Pixel rot gefaerbt
            if (ImageData[index] > threshold) {
                image.setPixel(255 - z, y, qRgb(0, 255, 255));  //255 - z, um knie links, huefte rechts
            } else {
                // Ansonsten wie zuvor (mit Input der Slider) gefensterter Grauwert
                windowcheck = MyLib::windowing(ImageData[index], windowstart, windowwidth, iGrauwert);
                // simpler Fehlercheck, -1 und -2 nicht ausdifferenziert - genauer nach
                if (windowcheck != 0)
                {
                    failcounter += 1;
                }
                image.setPixel((m_pData->gld - 1) - z, y, qRgb(iGrauwert, iGrauwert, iGrauwert));   //255 - z, um knie links, huefte rechts
            }
            index += m_pData->glw;
        }
    }
    if (failcounter > 0)
    {
        emit LOG(QTime::currentTime().toString() + ": There is an issue while applying the Windowing Function to the Sagittal View. The Start oder the Width value seems to be out of range.");
    }
    image = image.scaled(m_pData->gld * m_pData->pxspZ / m_pData->pxspXY, m_pData->glw * m_pData->pxspXY / m_pData->pxspXY);
    ui->label_image_sagittal->resize(image.width(), image.height());




    // Indicator Coronal in Sagittalview
    QPainter painter(&image);
    painter.setPen(QPen(Qt::yellow, 1, Qt::SolidLine , Qt::FlatCap));
    painter.drawLine(QPointF(0,coronalslice), QPointF(image.width(), coronalslice));

    // Indicator Transversal in Sagittalview
    painter.setPen(QPen(Qt::blue, 1, Qt::SolidLine , Qt::FlatCap));
    painter.drawLine(QPointF(transversalslice * m_pData->pxspZ/m_pData->pxspXY ,0), QPointF(transversalslice * m_pData->pxspZ/m_pData->pxspXY, image.height()));




    ui->label_image_sagittal->setPixmap(QPixmap::fromImage(image));
}

//-----------------------------------------------------------------------------------------------


// Auch für diese Ansichten kann getslice() verwendet werden, jedoch liefert scaled Funktion von QImage sauberer Kanten in der Ansicht
void View3D::update2DView_coronal(int transversalslice, int sagittalslice, int coronalslice, int windowstart, int windowwidth, int threshold)
{
    QImage image(m_pData->glw, m_pData->gld, QImage::Format_RGB32);
    short* ImageData = m_pData->getImage();

    int index = coronalslice * m_pData->glw;

    int windowcheck = 0;
    int failcounter = 0;

    int iGrauwert = 0;

    for (int z = 0; z < m_pData->gld; ++z) // Bildursprung oben links auessere Schleife von links nach rechts, innere Schleife von oben nach unten
    {
        for (int x = 0; x < m_pData->glw; ++x)
        {
            // falls Schwellwert ueberschritten (direkter Bezug auf HU nicht auf gefensterten Wert), wird betreffender Pixel rot gefaerbt
            if (ImageData[index] > threshold) {
                image.setPixel(x, z, qRgb(0, 255, 255));
            } else {
                // Ansonsten wie zuvor (mit Input der Slider) gefensterter Grauwert
                windowcheck = MyLib::windowing(ImageData[index], windowstart, windowwidth, iGrauwert);

                image.setPixel(x, z, qRgb(iGrauwert, iGrauwert, iGrauwert));
                if (windowcheck != 0)  // simpler Fehlercheck, -1 und -2 nicht ausdifferenziert
                {
                    failcounter += 1;
                }
            }
            index += 1;
        }
        index += (m_pData->glw - 1) * m_pData->glh;
    }
    if (failcounter > 0)
    {
        emit LOG(QTime::currentTime().toString() + ": There is an issue while applying the Windowing Function to the Coronal View. The Start oder the Width value seems to be out of range.");
    }
    // Spacing beim Anzeigen beachten Bild - > Reskalierung ueber QImage Scale Funktion sodass ein Pixelabstand 1.2695mm entspricht dh in diesem Fall etwa 403 Pixel in der Hoehe
    image = image.scaled(m_pData->glw * m_pData->pxspXY / m_pData->pxspXY, m_pData->gld * m_pData->pxspZ / m_pData->pxspXY);
    ui->label_image_coronal->resize(image.width(), image.height());


    // Indicator Transversal in Coronalview
    QPainter painter(&image);
    painter.setPen(QPen(Qt::blue, 1, Qt::SolidLine , Qt::FlatCap));
    painter.drawLine(QPointF(0,image.height()-transversalslice * m_pData->pxspZ / m_pData->pxspXY ), QPointF(image.width(), image.height()-transversalslice * m_pData->pxspZ / m_pData->pxspXY));

    // Indicator Sagittal in Coronalview
    painter.setPen(QPen(Qt::green, 1, Qt::SolidLine , Qt::FlatCap));
    painter.drawLine(QPointF(sagittalslice ,0), QPointF(sagittalslice, image.width()));


    ui->label_image_coronal->setPixmap(QPixmap::fromImage(image));
}
