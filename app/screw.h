#ifndef SCREW_H
#define SCREW_H

#include <QWidget>
#include <QTime>
#include "applicationdata.h"
#include "mylib.h"
#include <QtGui>


namespace Ui {
class screw;
}

class screw : public QWidget
{
    Q_OBJECT

public:
    explicit screw(QWidget *parent = nullptr);
    ~screw();

    void setData(ApplicationData* pData);



signals:
    void LOG(QString str);

private:
    Ui::screw *ui;
    ApplicationData *m_pData;

    void updateScrewView(int slice, float scale, int diameter, int WindowStart, int WindowWidth, int threshold);
    void updateOrthogonalScrewView(int slice, float scale, int diameter, int WindowStart, int WindowWidth, int threshold);


private slots:

    void updatedWindowingStart(int value);
    void updatedWindowingWidth(int value);

    void updatedThresh(int value);

    void updatedDiameter(int diameter);

    void updatedScale(int scale);

    void updatedSlice_Screw(int slice);



};

#endif // SCREW_H
