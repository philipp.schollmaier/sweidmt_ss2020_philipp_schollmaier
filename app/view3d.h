#ifndef VIEW3D_H
#define VIEW3D_H

#include <QWidget>
#include <applicationdata.h>
#include <QMouseEvent>
#include <QMouseEvent>
#include <QEvent>
#include "mylib.h"
#include <QTime>
#include <QtGui>

namespace Ui {
class View3D;
}

class View3D : public QWidget
{
    Q_OBJECT

public:
    explicit View3D(QWidget *parent = nullptr);
    ~View3D();

    void setData(ApplicationData* pData);

    void setInitializationValues();

private:
    Ui::View3D *ui;

    ApplicationData *m_pData;

    void updateViews(int transversalslice, int sagittalslice, int coronalslice, int windowstart, int windowwidth, int threshold);
    void update2DView_transversal(int transversalslice, int sagittalslice, int coronalslice, int windowstart, int windowwidth, int threshold);
    void update2DView_sagittal(int transversalslice, int sagittalslice, int coronalslice, int windowstart, int windowwidth, int threshold);
    void update2DView_coronal(int transversalslice, int sagittalslice, int coronalslice, int windowstart, int windowwidth, int threshold);

    void mousePressEvent(QMouseEvent *e);


private slots:

    void updatedWindowingStart(int value);
    void updatedWindowingWidth(int value);
    void updatedThresh(int value);

    void updatedSlice_transversal(int value);
    void updatedSlice_sagittal(int value);
    void updatedSlice_coronal(int value);

    void setStartPos();
    void setEndPos();


signals:
    void LOG(QString str);


};

#endif // VIEW3D_H
