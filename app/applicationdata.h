#ifndef APPLICATIONDATA_H
#define APPLICATIONDATA_H
#include <QWidget>
#include <QFile>
#include <iostream>
#include "mylib.h"

class ApplicationData
{
public:
    ApplicationData();
    ~ApplicationData();
    short* getImage();
    const short* getDepthMap();
    bool uploadImage(QString path);
    bool calculationDepthMap(int threshold);

    void setStartPosition(Eigen::Vector3d StartPosition);
    Eigen::Vector3d getStartPosition();

    void setEndPosition(Eigen::Vector3d EndPosition);
    Eigen::Vector3d getEndPosition();

    void setposavailable(int length);
    bool getposeavailable();

    // Voruebergehend; const short pointer in getImage inkompatibel zu Vorgabe in .dll; Weg abklaeren
    short* getImage2();


    // Globale Informationen zu Inputdaten festgelegt
    const int glw = 512; // globalwidth
    const int glh = 512; // globalheight
    const int gld = 256; // globaldepth
    const float pxspXY = 1.2695; // globalpixelspacing in xy-Richtung
    const float pxspZ = 2.0; // globalpixelspacing in z-Richtung

    //
    image3D m_pImage3D;


private:
    short* m_pTiefenkarte;
    short* m_pImageData;

    Eigen::Vector3d m_Startposition;
    Eigen::Vector3d m_Endposition;
    bool posavailable = false;




};

#endif // APPLICATIONDATA_H
