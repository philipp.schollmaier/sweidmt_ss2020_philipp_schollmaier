#include "mylib.h"

MyLib::MyLib()
{
}


int MyLib::windowing(int HU_value, int startValue, int windowWidth, int& greyValue)
{
    if ((HU_value < -1024) || (HU_value > 3071))
    {
        return -1;  // HU_value ist out of range
    }
    else if ((startValue < -1024) || (startValue > 3071) || (windowWidth < 1) || (windowWidth > 4096))
    {
        return -2; // windowing parameters are out of range
    }
    else
    {
        if (HU_value < startValue)
        {
            greyValue = 0;
        }
        else if (HU_value > (startValue + windowWidth))
        {
            greyValue = 255;
        }
        else
        {
            greyValue =  (HU_value - startValue)*255.0/windowWidth;
        }

        return 0;  // windowing ok
    }
}



/**
* \brief Calculates an arbitrary slice
* *
Calculates an arbitrary slice from within the original dataset image with the
reconstruction parameters param.
* \param image HU original dataset
* \param param reonstruction parameters
* \param im2D reconstructed 2d image
* *
\return 0 if ok. -1 if input image is incorrect. -2 if output im2D is incorrect.
*/
int MyLib::getSlice(const image3D& image, const Reconstruction& param, image2D& im2D)
{
    Eigen::Vector3d v_mm;
    Eigen::Vector3d v_vox;
    Eigen::Vector3d spacing(image.pixelSpacingXY, image.pixelSpacingXY, image.pixelSpacingZ);
    Eigen::Vector3d inv_spacing(1/image.pixelSpacingXY, 1/image.pixelSpacingXY, 1/image.pixelSpacingZ);

    short neighbor[2][2][2]; // 2x2x2 Array, Grauwerte der 8 nächsten Nachbarn, intuitive Anordnung der HU-Werte im 3d Array
    int x,y,z;

    float xrel, yrel, zrel; // für trilineare Interpolation gemaess https://en.wikipedia.org/wiki/Trilinear_interpolation
    short c_yz[2][2];
    short c_z[2];
    short c;


    // nicht isotrop verteilte HU-Werte muessen auf abitary Ebene(Bild) mit festem Pixelabstand mit trilin Interpol. gebracht werden

    // Ueber jeden Punkt der 2dBildebene iterieren und jeweils die korrespondierende 8 nächste Nachbarn
    for (int h = -floor(im2D.height/2) ; h < ceil(im2D.height/2); ++h) // bei geraden Pixelweiten/hoehen 1 Pixel Offset, da dann kein expliziter Mittelpunkt existiert
    {
        for (int w = -floor(im2D.width/2); w < ceil(im2D.width/2); ++w)
        {
            // Input ist Voxelposition, in der inheraent bspw ein Schritt in x-Ri 1.2695mm impliziert ist
            //
            // Ausgangspunkt in mm und dann entsprechende 1mm(abh vom Scaling) Schritte in neue x und y direction
            v_mm << (param.pos.cwiseProduct(spacing) + (1/param.scale) * float(w) * param.xdir.cwiseProduct(spacing).normalized() + (1/param.scale) * float(h) * param.ydir.cwiseProduct(spacing).normalized()); // normalized für schrittweite 1
            v_vox = v_mm.cwiseProduct(inv_spacing); // in Voxeln
            //v << (param.pos + (1/param.scale) * float(w) * param.xdir.normalized() + (1/param.scale) * float(h) * param.ydir.normalized());
            // Anpassen des Vektors sodass 1mm einem Pixelabstand entspricht
            //v << v.cwiseProduct(spacing);

            // hier abfangen, falls v außerhalb des Volumens befindlich ist
            // PRUEFEN, Randproblematik fuehrte bei nicht ortogonalen Vektoren xdir ydir zum Fehler
            //if (v[0] >= float(image.width - 1)  || v[1] >= float(image.height - 1) || v[2] >= float(image.slices - 1) || v[0] <= 1 || v[1] <= 1 || v[2] <= 1 )
            if (v_vox[0] > image.width - 1 || v_vox[1] > image.height - 1 || v_vox[2] > image.slices - 1 || v_vox[0] < 0 || v_vox[1] < 0 || v_vox[2] < 0 )
            {
                int index =  h * im2D.width + floor(im2D.height/2)* im2D.width + w + floor(im2D.width/2);
                im2D.pImage[index] = -1023; // HU-Wert für außerhalb des Volumens festgelegt möglichst niedrig, wegen pixelbasierten Segmentierung
            } else {

                // Naechsten 8 Nachbarn auffinden
                x = floor(v_vox[0]);
                y = floor(v_vox[1]);
                z = floor(v_vox[2]);
                neighbor[0][0][0] = image.pImage[z * image.width * image.height + y * image.width + x];

                x = ceil(v_vox[0]);
                y = floor(v_vox[1]);
                z = floor(v_vox[2]);
                neighbor[1][0][0] = image.pImage[z * image.width * image.height + y * image.width + x];


                x = floor(v_vox[0]);
                y = ceil(v_vox[1]);
                z = floor(v_vox[2]);
                neighbor[0][1][0] = image.pImage[z * image.width * image.height + y * image.width + x];

                x = floor(v_vox[0]);
                y = floor(v_vox[1]);
                z = ceil(v_vox[2]);
                neighbor[0][0][1] = image.pImage[z * image.width * image.height + y * image.width + x];

                x = ceil(v_vox[0]);
                y = ceil(v_vox[1]);
                z = floor(v_vox[2]);
                neighbor[1][1][0] = image.pImage[z * image.width * image.height + y * image.width + x];

                x = ceil(v_vox[0]);
                y = floor(v_vox[1]);
                z = ceil(v_vox[2]);
                neighbor[1][0][1] = image.pImage[z * image.width * image.height + y * image.width + x];

                x = floor(v_vox[0]);
                y = ceil(v_vox[1]);
                z = ceil(v_vox[2]);
                neighbor[0][1][1] = image.pImage[z * image.width * image.height + y * image.width + x];

                x = ceil(v_vox[0]);
                y = ceil(v_vox[1]);
                z = ceil(v_vox[2]);
                neighbor[1][1][1] = image.pImage[z * image.width * image.height + y * image.width + x];



                // Relatives KOS im Quader bezogen auf einen Knotenpunkt
                // Berücksichtigen, dass Nachbarschaftsquader nicht isotrop ist, für "korrekte Gewichtung" der Nachbar HU-Werte
                xrel = (v_vox[0] - floor(v_vox[0]));
                yrel = (v_vox[1] - floor(v_vox[1]));
                zrel = (v_vox[2] - floor(v_vox[2]));

                // Trilineare Interpol gemaess https://en.wikipedia.org/wiki/Trilinear_interpolation
                // https://en.wikipedia.org/wiki/Trilinear_interpolation#/media/File:3D_interpolation2.svg
                // Korrespondierende Punkte auf 4 Kanten
                c_yz[0][0] = 1 * neighbor[0][0][0]*(1 - xrel) + neighbor[1][0][0] * xrel;
                c_yz[0][1] = 1 * neighbor[0][0][1]*(1 - xrel) + neighbor[1][0][1] * xrel;
                c_yz[1][0] = 1 * neighbor[0][1][0]*(1 - xrel) + neighbor[1][1][0] * xrel;
                c_yz[1][1] = 1 * neighbor[0][1][1]*(1 - xrel) + neighbor[1][1][1] * xrel;

                // Korrespondierende Punkte auf Verbindungslinie  der Punkte in einer Ebene
                c_z[0] = c_yz[0][0]*(1 - yrel) + c_yz[1][0] * yrel;
                c_z[1] = c_yz[0][1]*(1 - yrel) + c_yz[1][1] * yrel;

                // Neuer interpolierten Grauwert für die abitary plane an Stelle w und h
                c = c_z[0]*(1 - zrel) + c_z[1] * zrel;


                // Index auffinden, in dem Grauwert eingetragen wird
                // UEBERPRUEFEN
                //int index = ( (h + ceil(im2D.height/2)) * im2D.width + w + ceil(im2D.width/2) );
                int index =  h * im2D.width + floor(im2D.height/2)* im2D.width + w + floor(im2D.width/2);
                im2D.pImage[index] = c;
            }

        }

    }
    return 0;
}
