#ifndef MYLIB_H
#define MYLIB_H

#include "MyLib_global.h"
#include "Eigen/Core"
#include "Eigen/Geometry"  //3DVektoren werden mit Eigen::Vector3d vec; instanziiert und 3DMatrizen mit Eigen::Matrix3d mat;
#include <math.h>

struct image3D
{
    int width;
    int height;
    int slices;
    double pixelSpacingXY;
    double pixelSpacingZ;
    short* pImage;
} ;

struct image2D
{
public:
    image2D(int w, int h)
    {
        pImage = new short[w*h];
        width = w;
        height = h; // inline constructor definition
        memset(pImage, 0, w*h*sizeof(short));           // speicherbereich wird direkt mit 0en gefuellt uebergeben
    };
    ~image2D(){delete pImage;};
    int width;
    int height;
    short* pImage;
};

struct Reconstruction
{
    Eigen::Vector3d pos;
    Eigen::Vector3d xdir;
    Eigen::Vector3d ydir;
    double scale;
};



class MYLIB_EXPORT MyLib
{
public:
    MyLib();


    /**
    * \brief Calculates a windowed pixel
    * *
    Calculates a windowed pixel from the HU_values of the original
    dataset based on the windowing parameters.
    * \param HU_value HU value to be windowed
    * \param startValue Start value for windowing. [-1024..3072]
    * \param windowWidth Width value for windowing. [1..4096]
    * \param greyValue changed via call by reference
    * \return 0 if ok. -1 if HU_value is out of range. -2 if windowing
    parameters are out of range.
    */
    static int windowing(int HU_value, int windowStart, int windowWidth, int& greyValue); //Deklaration als Static, somit ohne explizite Erstellung eines Objekts der Klasse MyLib

    /**
    * \brief Calculates an arbitrary slice
    * *
    Calculates an arbitrary slice from within the original dataset image with the
    reconstruction parameters param.
    * \param image HU original dataset
    * \param param reonstruction parameters
    * \param im2D reconstructed 2d image
    * *
    \return 0 if ok. -1 if input image is incorrect. -2 if output im2D is incorrect.
    */
    static int getSlice(const image3D& image, const Reconstruction& param, image2D& im2D);


};

#endif // MYLIB_H
